<?php

/**
 * Get Dachcom PageDesigner Core Settings and Options
 *
 * Maybe we have to move this file to /admin, if no frontEnd Settings will come.
 *
 * @since      1.0.0
 * @package    Dachcom_Page_Designer_Settings
 * @subpackage Dachcom_Page_Designer_Settings/includes
 * @author     Stefan Hagspiel <shagspiel@dachcom.com>
 */
class Dachcom_Page_Designer_Settings {

    public function __construct() { }

    public function get_default_classes() {

        $theme_columns = get_option('dpd_setting-theme_column');

        $default_col_classes = array(

            'oneCol'        => 'col-xs-' . $theme_columns . ' col-ty-12 col-sm-1 col-md-1 col-lg-1',
            'twoCol'        => 'col-xs-' . $theme_columns . ' col-ty-12 col-sm-2 col-md-2 col-lg-2',
            'threeCol'      => 'col-xs-' . $theme_columns . ' col-ty-12 col-sm-3 col-md-3 col-lg-3',
            'fourCol'       => 'col-xs-' . $theme_columns . ' col-ty-12 col-sm-4 col-md-4 col-lg-4',
            'fiveCol'       => 'col-xs-' . $theme_columns . ' col-ty-12 col-sm-5 col-md-5 col-lg-5',
            'sixCol'        => 'col-xs-' . $theme_columns . ' col-ty-12 col-sm-6 col-md-6 col-lg-6',
            'sevenCol'      => 'col-xs-' . $theme_columns . ' col-ty-12 col-sm-7 col-md-7 col-lg-7',
            'eightCol'      => 'col-xs-' . $theme_columns . ' col-ty-12 col-sm-8 col-md-8 col-lg-8',
            'nineCol'       => 'col-xs-' . $theme_columns . ' col-ty-12 col-sm-9 col-md-9 col-lg-9',
            'tenCol'        => 'col-xs-' . $theme_columns . ' col-ty-12 col-sm-10 col-md-10 col-lg-10',
            'elevenCol'     => 'col-xs-' . $theme_columns . ' col-ty-12 col-sm-11 col-md-11 col-lg-11',
            'twelveCol'     => 'col-xs-' . $theme_columns . ' col-ty-12 col-sm-12 col-md-12 col-lg-12'

        );

        return apply_filters( 'dachcom-page-designer/get_column_class', $default_col_classes );

    }

}