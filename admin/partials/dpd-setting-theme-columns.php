<?php $option = get_option('dpd_setting-theme_column'); ?>

<select name="dpd_setting-theme_column">
    <option value="null"><?php _e('ohne', 'dachcom-page-designer'); ?></option>
    <option value="4"<?php echo selected( $option, '4', false); ?>><?php _e('4-Spaltig', 'dachcom-page-designer'); ?></option>
    <option value="6"<?php echo selected( $option, '6', false); ?>><?php _e('6-Spaltig', 'dachcom-page-designer'); ?></option>
    <option value="8"<?php echo selected( $option, '8', false); ?>><?php _e('8-Spaltig', 'dachcom-page-designer'); ?></option>
    <option value="10"<?php echo selected( $option, '10', false); ?>><?php _e('10-Spaltig', 'dachcom-page-designer'); ?></option>
    <option value="12"<?php echo selected( $option, '12', false); ?>><?php _e('12-Spaltig', 'dachcom-page-designer'); ?></option>
</select>
