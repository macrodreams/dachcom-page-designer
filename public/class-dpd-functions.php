<?php

/**
 * Public Functions
 *
 */

/**
 * @param string $colSize
 *
 * @return bool
 */
function dpd_get_page_designer_column_class( $colSize ){

    $settings = new Dachcom_Page_Designer_Settings();

    $defaultClasses = $settings->get_default_classes();

    if( isset( $defaultClasses[ $colSize ] ) )
        return $defaultClasses[ $colSize ];

    return FALSE;

}

function dpd_is_row_break( $block_counter, $col_counter ) {

    if( $block_counter == 0 )
        return true;

    $theme_columns = get_option('dpd_setting-theme_column');

    $max_column = $theme_columns;

    if( $col_counter % $max_column == 0 || $col_counter > $max_column )
        return true;

    return false;

}

function dpd_calculate_row_count( $column_size ) {

    $break = 0;

    switch( $column_size ) {

        case 'oneCol' : $break += 1;
            break;
        case 'twoCol' : $break += 2;
            break;
        case 'threeCol' : $break += 3;
            break;
        case 'fourCol' : $break += 4;
            break;
        case 'fiveCol' : $break += 5;
            break;
        case 'sixCol' : $break += 6;
            break;
        case 'sevenCol' : $break += 7;
            break;
        case 'eightCol' : $break += 8;
            break;
        case 'nineCol' : $break += 9;
            break;
        case 'tenCol' : $break += 10;
            break;
        case 'elevenCol' : $break += 11;
            break;
        case 'twelveCol' : $break += 12;
            break;
    }

    return $break;

}

function dpd_col_int_to_string( $colInt ) {

    $break = '';

    switch( (int) $colInt ) {

        case 1 : $break = 'oneCol';
            break;
        case 2 : $break = 'twoCol';
            break;
        case 3 : $break = 'threeCol';
            break;
        case 4 : $break = 'fourCol';
            break;
        case 5 : $break = 'fiveCol';
            break;
        case 6 : $break = 'sixCol';
            break;
        case 7 : $break = 'sevenCol';
            break;
        case 8 : $break = 'eightCol';
            break;
        case 9 : $break = 'nineCol';
            break;
        case 10 : $break = 'tenCol';
            break;
        case 11 : $break = 'elevenCol';
            break;
        case 12 : $break = 'twelveCol';
            break;
    }

    return $break;
}

/* Module API */

function dpd_render_module_view( $params = array() ) {

    $rendererClass = new Dachcom_Page_Designer_RenderEngine();

    $str = $rendererClass->render( $params );

    return $str;

}

function dpd_camelcase_to_underscore( $string, $getterSetter = FALSE  ) {

    preg_match_all('!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!', $string, $matches);
    $ret = $matches[0];
    foreach ($ret as &$match) {
        $match = $match == strtoupper($match) ? strtolower($match) : lcfirst($match);
    }

    $t = implode('_', $ret);

    if( $getterSetter)
        $t = str_replace(array('get_','set_'),array('get','set'), $t);

    return $t;

}