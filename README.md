Dachcom Page Designer
================================

Description
-------------
Dachcom Page Designer is the missing link to create powerfull webpages in a second.
It's not build on thousands of shortcodes so every entry is stored as a post_meta value.

Preview: http://recordit.co/udTPhnSBVe

Wiki
-------------
Please read the WIKI for further informations: https://bitbucket.org/dachcom/dachcom-page-designer/wiki/Home

Requirements
-------------
* [ACF Pro](http://www.advancedcustomfields.com/pro/): Yes, you need to buy it. But it will change your life. I promise!
* Some PHP & JS Skills


Installation
-------------

1. upload to the `/wp-content/plugins/` directory
2. activate the plugin through the 'Plugins' menu in WordPress
3. create a folder called *page_designer* in your theme directory
4. to get some samples to start, unzip *pd_examples.zip* from the plugin directory:
4. - move the ``Image`` folder into your page_designer folder.
4. - move the ``page-designer.php`` file into your theme folder.
4. - tell page designer, that your using the ``page-designer.php`` as your new template (see ``dachcom-page-designer/page-designer-location`` filter)
5. to display page designer data in frontend, you need to add an helper: ``<?php echo dpd_render_module_view(); ?>``

Filters
-------------

### Page Designer Appearance

It's a good practice to create a page template which tells ACF to load *Page Designer*.
After you have created an Template, you need to tell *Page Designer* about it.

Add to your functions.php:

``add_filter('dachcom-page-designer/page-designer-location', 'set_page_designer_location', 10, 1);``

```
function set_page_designer_location( $location ) {

    //add the demo page to location
    $location[] = array(
        array(
            'param' => 'page_template',
            'operator' => '==',
            'value' => 'page-designer.php', //your template slug name.
            'order_no' => 0,
            'group_no' => 0,
        ),
    );

    return $location;

```
	
Changelog
-------------

##### 1.8.2
* Page Designer is now working with ACF PRO 5.3.3. Please Update!
* Readme Update

##### 1.8.1
* Image Module Example fix
* Added US Localization (Thanks to Alec Rippberger)
* Instruction fixes

##### 1.8.0
* Created Themosis / Default Theme Abstraction
* introducing Config/Controller/Model Concept

##### 1.7.1
* Remove must-dependency to dachcom plugin observer, so other users can use it.
* Went Open Source

##### 1.7.0
* Updated for ACF 5.3.1
* Important: Only update, if you're using ACF 5.3.1
* Added smart RowHeight in Backend
* Performance Fixes
* uniform Naming

##### 1.6.1
* Updated for ACF 5.2.8.
* Important: Only update, if you're using ACF 5.2.8

##### 1.6
* Improved JS code
* Better Module Handling

##### 1.5
* Added to Dachcom Environment
* huge performance updates
* code improvements
* required acf pro 5.2.4

##### 1.4.7
* CSS Bug

##### 1.4.6
* Now compatible with acf 5.0.8

##### 1.4.5
* Updated Layout width

##### 1.4.4
- Added ACF 5.0.5 Compatibility

##### 1.4.3
* First Row Layout fixed

##### 1.4.2
* Huge Performance Updates
* Added Undo Support
* Layout Improvements
* TinyMCE Height Improvements

##### 1.4.1
- Rowlayout Bug fixed.

##### 1.4.0 =
- Released ACF 5.0 Support. This Version is not backwards compatible.